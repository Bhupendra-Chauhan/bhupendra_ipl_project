const fs = require("fs");


const JSON_OUTPUT_FILE_PATH = "../public/output/";
const ipl = require('./ipl');
//const NumberOfMatchesWon = require("./ipl");
const convertCSVtoJSON = require('./convertCSVtoJSON');



async function Data_in() {

    const matches = await convertCSVtoJSON('matches.csv');
    const deliveries = await convertCSVtoJSON('deliveries.csv');
    let result = Head(matches, deliveries);

    data_output(result);
}

Data_in();


function Head(matches, deliveries){
   let result={};
   result.MatchesPLayedPerYear=ipl.MatchesPLayedPerYear(matches);
   result.NumberOfMatchesWon=ipl.NumberOfMatchesWon(matches);
   result.ExtraRunsConcededPerTeamIn2016=ipl.ExtraRunsConcededPerTeamIn2016(matches,deliveries);
   result.Top10EconomicalBowlersIn2015=ipl.Top10EconomicalBowlersIn2015(matches,deliveries);
   return result;
    
}

function data_output(result){
    for (let i in result) {
        let jsonString = JSON.stringify(result[i]);
        fs.writeFile(`${JSON_OUTPUT_FILE_PATH}${i}.json `, jsonString, "utf8", err => {
            if (err) {
                console.error(err);
            }
        });
    }

}

