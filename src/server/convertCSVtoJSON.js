const csv = require("csvtojson");
const path = require('path')

async function convertCSVtoJSON(fileName) {
    const data = await csv().fromFile(path.join(__dirname, `../data/${fileName}`))
    return data
}

module.exports=convertCSVtoJSON;
