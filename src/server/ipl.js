//Q.1  Number of matches played per year for all the years in IPL.

function MatchesPLayedPerYear(matches){
    const result={};
    for(let match of matches){
        const season=match.season;
        if(result[season]){
            result[season] += 1;
        }
        else{
            result[season] = 1;
        }
    }
    return result;
}



//Q.2  Number of matches won per team per year in IPL.

  function NumberOfMatchesWon(matches){
     const result = {};
    for (let match of matches) {
        const year = match.season;
        const winner = match.winner;
        if (result[year]) {
            if (result[year][winner]) {
                result[year][winner] += 1;
            } else {
                result[year][winner] = 1;
            }
        } else {
            result[year] = {};
            result[year][winner] = 1;
        }
    }
    return result;

} 





//Q.3   Extra runs conceded per team in the year 2016

function ExtraRunsConcededPerTeamIn2016(matches,deliveries) {
    const result = {};
    const delivery_id= matches.filter(obj => obj['season'] === '2016').map(obj => parseInt(obj.id));
    const deliveries_of_2016 = deliveries.filter(del => delivery_id.includes(parseInt(del['match_id'])));
    for (let id of delivery_id) {
        for (let delivery of deliveries_of_2016) {
            if (parseInt(delivery['match_id']) === id) {
                if (result[delivery.bowling_team]) {
                    result[delivery.bowling_team] += parseInt(delivery['extra_runs']);
                } else {
                    result[delivery.bowling_team] = parseInt(delivery['extra_runs']);
                }
            }
        }
    }

    return result;
}









//Q.4  Top 10 economical bowlers in the year 2015



function Top10EconomicalBowlersIn2015(matches,deliveries) {

    const result = {};

    const ids = matches.filter(obj => obj['season'] === '2015').map(obj => parseInt(obj.id));
    const deliveries_2015 = deliveries.filter(del => ids.includes(parseInt(del['match_id'])));
    for (let id of ids) {
        for (let delivery of deliveries_2015) {
            if (parseInt(delivery['match_id']) === id) {
                if (result[delivery.bowler]) {
                    result[delivery.bowler].runConceded += parseInt(delivery['total_runs']);
                    result[delivery.bowler].bowls += 1;
                } else {
                    result[delivery.bowler] = {};
                    result[delivery.bowler].runConceded = parseInt(delivery['total_runs']);
                    result[delivery.bowler].bowls = 1;
                }
            }
        }
    }

    let arr = [];

    for (let bowler in result) {

        const economyRate = +((result[bowler].runConceded / (result[bowler].bowls) / 6).toFixed(2));
        result[bowler].economyRate = economyRate;
        arr.push([bowler, result[bowler].economyRate]);

    }

    arr.sort(function(a, b) {
        return a[1] - b[1];
    });

    var top10EconomicalBowler = {};

    arr.slice(0, 10).forEach(key => top10EconomicalBowler[key[0]] = key[1]);

    return top10EconomicalBowler;
}





module.exports={
    MatchesPLayedPerYear,
    NumberOfMatchesWon,
    ExtraRunsConcededPerTeamIn2016,
    Top10EconomicalBowlersIn2015
}